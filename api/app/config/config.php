<?php
    // App  Root
    define('APPROOT', dirname(dirname(__FILE__))); //dirname me da el direc padre

    // Url Root
    define('URLROOT', 'http://3.222.3.210:80/qaraywa');

    // Site name
    define('SITENAME', 'QARAYWA');

    // DB Params
    define('DB_HOST','localhost');
    define('DB_USER','postgres');
    define('DB_PASS','postgres');
    define('DB_NAME','postgres');