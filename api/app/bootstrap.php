<?php
    // cargar los valores predeterminados para la app
    require_once 'config/config.php';

    //cargar las clases del paquete "libraries"
    //require_once 'libraries/core.php';
    //require_once 'libraries/controller.php';
    //require_once 'libraries/database.php';

    //autoload libraries 
    spl_autoload_register(function($className){
        require_once 'libraries/' . $className . 'php';
    });