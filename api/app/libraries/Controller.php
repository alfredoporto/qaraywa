<?php
    /*
     *Base controller
     *Carga  los models y views
     */
    class Controller{
        //carga model
        public function model($model){
            require_once '../app/models/' . $model . 'php';

            return new $model();
        }

        public function view($view, $data=[]){
            if(file_exists('../app/views' . $view . 'php')){
                require_once '../app/views' . $view . 'php';
            }else{
                die('La vista no existe');
            }
        }
    }