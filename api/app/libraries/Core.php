<?php
/* 
 *App core API class
 *Crea URLs y carga el core controller
 *Formato URL - /controller/method/params
 */

class Core{
    protected $currentController = 'FirstTimeUser';
    protected $currentMethod = 'index';
    protected $params = [];

    public function __construct()
    {
        //print_r($this->getUrl());
        $url = $this->getUrl();

        //buscará el primer valor de la url en los controllers existentes
        if(file_exists('../app/controllers' . ucwords($url[0]). '.php')){ //ucwords es para poner la primera palabra en mayuscula || la ruta se hace con
                                                                            //index como pwd
            //si existe, ponerlo como controller
            $this->currentController = ucwords($url[0]);
            //unset 0 Index
            unset($url[0]);
        }

        //lo requiero el controller
        require_once '../app/controllers/'.$this->currentController .'.php';
        //lo instancio
        $this->currentController = new $this->currentController;

        //chequear por el segundo item del url
        if(isset($url[1])){
            // ver si existe el metodo dentro del controller
            if(method_exists($this->currentController, $url[1])){
                $this->currentMethod = $url[1];
                unset($url[1]);
            }
        }

        //capturar los parametros
        $this->params = $url ? array_values($url) : [];

        //llamar a un callback con el arreglo de params
        call_user_func_array([$this->currentController,$this->currentMethod],$this->params);
    }



    public function getUrl()
    {
        //echo $_GET['url'];
        if(isset($_GET['url'])){
            $url = rtrim($_GET['url'],'/');
            $url = filter_var($url, FILTER_SANITIZE_URL);
            $url = explode('/',$url);
            return $url;
        }
    }
}
